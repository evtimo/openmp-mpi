/*
7. Реализовать рассылку массива значений на n процессов с помощью
двухточечных обменов по принципу: каждому процессу отправляется зара-
нее заданное число значений.
*/

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 8
#define BSIZE 2

int main(int argc, char* argv[]) {

  int rank, size;
  int mas[N] = {1,2,3,4,5,6,7,8};

  MPI_Status status;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &size);

  char recieved[BSIZE];

  if (rank == 0) {
    int i;
    for (i = 0; i < N; i++) {
      MPI_Send(&mas[i*BSIZE], BSIZE, MPI_INT, i+1, 0, MPI_COMM_WORLD);
    }
  } else {
    MPI_Recv(&recieved, BSIZE, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }

  MPI_Finalize();
  return 0;
}

// stats
// real 0.14 0.15 0.14 0.15 -> avg = 0.145
// user 0.20 0.21 0.18 0.20 
// sys  0.17 0.16 0.12 0.12
