lab6:
	gcc -fopenmp lab6.c -o lab6 -std=c99
lab7.1:
	gcc -fopenmp lab7.1.c -o lab7.1 -std=c99 -lm
lab7.2:
	gcc -fopenmp lab7.2.c -o lab7.2 -std=c99 -lm
lab9:
	mpicc lab9.c -o lab9
	mpirun -np 5 ./lab9
lab10:
	mpicc lab10.c -o lab10
	mpirun -np 9 ./lab10
lab11:
	mpicc lab11.c -o lab11
	mpirun -np 9 ./lab11
lab10-11:
	time -p mpirun -np 9 ./lab10
	time -p mpirun -np 9 ./lab11
lab12:
	mpicc lab12.c -o lab12
	mpirun -np 27 ./lab12

