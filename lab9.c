// 6. Первая задача про деньги. Десять бухгалтеров сводят годовой ба-
// ланс. Каждый из них отвечает за свою сторону жизни предприятия – зарпла-
// ту, материальные расходы и т.п., и точно знает, сколько доходов и расходов
// числится за ним. Бухгалтера заполняют баланс по очереди – сперва первый
// из них заполняет свой раздел, затем это делает второй и т.д. В конце концов
// все попадает на стол главному бухгалтеру и он объявляет результат и везет
// отчет в налоговую инспекцию. Написать программу, моделирующую пове-
// дение бухгалтеров, используя метод передачи информации «точка-точка».
// 7. Вторая задача про деньги. Решить задачу 6, но при условии, что
// все данные в отчет заносит главный бухгалтер, а сотрудники просто подхо-
// дят к нему по одному и сообщают свои цифры.

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <mpi.h>

<<<<<<< HEAD
#define BUH 4
=======
#define BUH 10
>>>>>>> 1ea1860f097bac3a040668cd880b262987c13c9a

int getIncomeFromBuh(int i) {
    return (rand() % 1000) * (i+1);
}

int getOutcomeFromBuh(int i) {
    return (rand() % 1000) * (i+1);
}

int main(int argc, char* argv[]) {
  int rank, size, i;
//  srand(time(NULL) + rank);
  MPI_Status status;

  MPI_Init(&argc, &argv); // инициализация MPI библиотеки
  MPI_Comm_rank(MPI_COMM_WORLD, &rank); // определение кол-ва запущенных ветвей
  MPI_Comm_size(MPI_COMM_WORLD, &size); // определение своего ранга
  double t=MPI_Wtime(); //фиксация времени начала

  int income, outcome, balance = 0;

  if (rank >=0 && rank < BUH) {
    income = getIncomeFromBuh(rank);
    outcome = getIncomeFromBuh(rank);
    MPI_Send(&income, 1, MPI_INT, BUH, 0, MPI_COMM_WORLD); // отправляем доходы главбуху
    MPI_Send(&outcome, 1, MPI_INT, BUH, 1, MPI_COMM_WORLD); // отправляем расходы главбуху
    printf("[%d/%d] Sent income = %d ; outcome = %d\n", rank, size-1, income, outcome);
  } else if (rank == BUH) {
     for (i = 0; i < BUH; i++) {
      MPI_Recv(&income, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // забираем информацию о доходах у каждого
      MPI_Recv(&outcome, 1, MPI_INT, i, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // забираем информацию о расходах у каждого 
      balance += income - outcome;
    }
    printf("Total balance = %d\n", balance);  

  } 

/* 
  for (i = 0; i < BUH; i++) {
    income = getIncomeFromBuh(i);
    outcome = getIncomeFromBuh(i);
    MPI_Sendrecv(&income, 1, MPI_INT, i, 0, recv, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    MPI_Sendrecv(&outcome, 1, MPI_INT, i, 0, recv, 1, MPI_INT, i, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    balance += income - outcome;
  }
  printf("Total balance = %d\n", balance); 
*/
  
  MPI_Finalize(); // закрытие MPI-библиотеки

  double t2=MPI_Wtime(); //фиксация времени завершения
  //printf("Time = %f\n", t2-t);

  return 0;
}
