/*
7. Эффективность реализации сравнить с функцией MPI_Scatterv().
*/

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

<<<<<<< HEAD
#define N 4
=======
#define N 3
>>>>>>> 1ea1860f097bac3a040668cd880b262987c13c9a

int main(int argc, char *argv[])
{
    int rank, size;         
    int i;
    
    int sum = 0;                
    char rec_buf[100];         

    int mas[N][N] = {{1,2,3},{4,5,6},{7,8,9}};
    
    
    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    int rem = (N*N)%size;
  
    int *sendcounts = malloc(sizeof(int)*size);
    int *displs = malloc(sizeof(int)*size);

    for (i = 0; i < size; i++) {
        sendcounts[i] = (N*N)/size;
        if (rem > 0) {
            sendcounts[i]++;
            rem--;
        }
        displs[i] = sum;
        sum += sendcounts[i];
    }

    MPI_Scatterv(&mas, sendcounts, displs, MPI_CHAR, &rec_buf, 100, MPI_CHAR, 0, MPI_COMM_WORLD);
    // sendcounts -целочисленный массив (размер равен числу процессов в группе), содержащий число элементов, посылаемых каждому процессу;
    // displs - целочисленный массив (размер равен числу процессов в группе), i-ое значение определяет смещение относительно начала sendbuf для данных, посылаемых процессу i;

    MPI_Finalize();
    free(sendcounts);
    free(displs);

    return 0;
}

// stats
// real 0.10 0.11 0.10 0.11 --> avg = 0.105
// user 0.18 0.23 0.19 0.22
// sys  0.17 0.13 0.14 0.15

