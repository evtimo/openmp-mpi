// 7. Задача о вычислении произведения матриц-3. Даны матрицы A
// размерностью n1 строк на n2 столбцов и матрица B размерностью n2 строк на
// n3 столбцов. Исходные данные хранятся в файлах на диске, подготовленные
// для каждого процессора (написать последовательную программу подготовки
// данных, входным параметром которой является число процессоров). Напи-
// сать программу для p процессоров (2<=p<<n1,n2,n3 , p%n1, p%n2, p%n3 =
// 0), вычисляющую произведение матриц AB, собирающую результат в 0-м
// процессоре, который сбрасывает его в файл. Распределение нагрузки на про-
// цессоры провести наиболее удобным способом. Использовать алгоритм с то-
// пологией «3D-решетка» и парадигмой управляющий-рабочий (в начале программы
// 0-й процессор считывает все данные и распределяет по процессорам).

#include<stdio.h>
#include<stdlib.h>
#include<mpi.h>
#include<time.h>
#include<sys/time.h>


#define NUM_DIMS 3
#define P0 3
#define P1 3
#define P2 3
#define M 500
#define N 500
#define K 500
#define A(i,j) A[N*i+j]
#define B(i,j) B[K*i+j]
#define C(i,j) C[K*i+j]
#define C1(i,j) C1[K*i+j]


int PMATMAT_3(n, A, B, C, p, comm)

  int *n;                 
  double *A, *B, *C;
  int *p;
  MPI_Comm comm;

{
  double *AA, *BB, *CC, *CC1;
  int nn[3];
  int coords[3];           
  int rank;                
  int *dispa, *dispb, *dispc, *counta, *countb, *countc;
  MPI_Datatype typea, typeb, typec, types[2];
  int periods[3], remains[3];
  MPI_Comm comm_3D, comm_2D[3], comm_1D[3], pcomm;
  int i, j, k, blen[2];
  MPI_Comm_dup(comm, &pcomm);
  MPI_Aint sizeofdouble, disp[2];
  MPI_Bcast(n, 3, MPI_INT, 0, pcomm);
  MPI_Bcast(p, 3, MPI_INT, 0, pcomm);
  for(i = 0; i < 3; i++) {
    periods[i] = 0;
  }
  MPI_Cart_create(pcomm, 3, p, periods, 0, &comm_3D);
  MPI_Comm_rank(comm_3D, &rank);
  MPI_Cart_coords(comm_3D, rank, 3, coords);
  for(i = 0; i < 3; i++) { 
    for(j = 0; j < 3; j++)
      remains[j] = (i != j);
      MPI_Cart_sub(comm_3D, remains, &comm_2D[i]);
  }
  for(i = 0; i < 3; i++) { 
    for(j = 0; j < 3; j++)
      remains[j] = (i == j);
      MPI_Cart_sub(comm_3D, remains, &comm_1D[i]);
  }

  for(i = 0; i < 3; i++) nn[i] = n[i]/p[i];

  #define AA(i,j) AA[nn[1]*i+j]
  #define BB(i,j) BB[nn[2]*i+j]
  #define CC(i,j) CC[nn[2]*i+j]
  AA = (double *)malloc(nn[0] * nn[1] * sizeof(double));
  BB = (double *)malloc(nn[1] * nn[2] * sizeof(double));
  CC = (double *)malloc(nn[0] * nn[2] * sizeof(double));
  if(rank == 0) {
    MPI_Type_vector(nn[0], nn[1], n[1], MPI_DOUBLE, &types[0]);
    MPI_Type_extent(MPI_DOUBLE, &sizeofdouble);
    blen[0] = 1;
    blen[1] = 1;
    disp[0] = 0;
    disp[1] = sizeofdouble * nn[1];
    types[1] = MPI_UB;
    MPI_Type_struct(2, blen, disp, types, &typea);
    MPI_Type_commit(&typea);
    dispa = (int *)malloc(p[0]*p[1] * sizeof(int));
    counta = (int *)malloc(p[0]*p[1] * sizeof(int));
    for(j = 0; j < p[0]; j++)
      for(i = 0; i < p[1]; i++) { 
        dispa[j*p[1]+i] = (j*p[1]*nn[0] +i);
          counta[j*p[1]+i] = 1;
    }
    MPI_Type_vector(nn[1], nn[2], n[2], MPI_DOUBLE, &types[0]);
    disp[1] = sizeofdouble*nn[2];
    MPI_Type_struct(2,blen,disp,types,&typeb);
    MPI_Type_commit(&typeb);
    dispb = (int *)malloc(p[1]*p[2] * sizeof(int));
    countb = (int *)malloc(p[1]*p[2] * sizeof(int));
    for(j = 0; j < p[1]; j++)
      for(i = 0; i < p[2]; i++) { 
        dispb[j*p[2]+i] = (j*p[2]*nn[1] +i);
        countb[j*p[2]+i] = 1;
    }
    MPI_Type_vector(nn[0], nn[2], n[2], MPI_DOUBLE, &types[0]);
    disp[1] = sizeofdouble*nn[2];
    MPI_Type_struct(2, blen, disp, types, &typec);
    MPI_Type_commit(&typec);
    dispc = (int *)malloc(p[0]*p[2] * sizeof(int));
    countc = (int *)malloc(p[0]*p[2] * sizeof(int));
    for(j = 0; j < p[0]; j++)
        for(i = 0; i < p[2]; i++) { 
            dispc[j*p[2]+i] = (j*p[2]*nn[0] +i);
            countc[j*p[2]+i] = 1;
        }   
    }
    if(coords[2] == 0) MPI_Scatterv(A, counta, dispa, typea, AA, nn[0]*nn[1], MPI_DOUBLE, 0, comm_2D[2]);
    if(coords[0] == 0) MPI_Scatterv(B, countb, dispb, typeb, BB, nn[1]*nn[2], MPI_DOUBLE, 0, comm_2D[0]);
    MPI_Bcast(AA, nn[0]*nn[1], MPI_DOUBLE, 0, comm_1D[2]);
    MPI_Bcast(BB, nn[1]*nn[2], MPI_DOUBLE, 0, comm_1D[0]);

    for(i = 0; i < nn[0]; i++)
        for(j = 0; j < nn[2]; j++) { 
            CC(i,j) = 0;
            for(k = 0; k < nn[1]; k++)
                CC(i,j) = CC(i,j) + AA(i,k) * BB(k,j);
        }
        CC1 = (double *)malloc(nn[0] * nn[2] * sizeof(double));
        MPI_Reduce(CC, CC1, nn[0]*nn[2], MPI_DOUBLE, MPI_SUM, 0, comm_1D[1]);
        if(coords[1] == 0) MPI_Gatherv(CC1, nn[0]*nn[2], MPI_DOUBLE, C, countc, dispc, typec, 0, comm_2D[1]);
        free(AA);
        free(BB);
        free(CC);
        free(CC1);
        MPI_Comm_free(&pcomm);
        MPI_Comm_free(&comm_3D);
        for(i = 0; i < 3; i++) { 
          MPI_Comm_free(&comm_2D[i]);
          MPI_Comm_free(&comm_1D[i]);
        }
        if(rank == 0) { 
          free(counta);
          free(countb);
          free(countc);
          free(dispa);
          free(dispb);
          free(dispc);
          MPI_Type_free(&typea);
          MPI_Type_free(&typeb);
          MPI_Type_free(&typec);
          MPI_Type_free(&types[0]);
        }
        return 0;
}

int main(int argc, char **argv) {
    double *A, *B, *C;
    int size, proc, n[3], p[3], i, j, k;
    int dims[NUM_DIMS], periods[NUM_DIMS];
    int reorder = 0;
    struct timeval tv1, tv2;
    int dt1;
    MPI_Comm comm;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &proc);
    for(i = 0; i < NUM_DIMS; i++) { 
        dims[i] = 0; 
        periods[i] = 0; 
    }
    MPI_Dims_create(size, NUM_DIMS, dims);
    MPI_Cart_create(MPI_COMM_WORLD, NUM_DIMS, dims, periods, reorder, &comm);
    n[0] = M;
    n[1] = N;
    n[2] = K;
    p[0] = P0;
    p[1] = P1;
    p[2] = P2;
    if(proc == 0) { 
        A = (double *)malloc(n[0] * n[1] * sizeof(double));
        B = (double *)malloc(n[1] * n[2] * sizeof(double));
        C = (double *)malloc(n[0] * n[2] * sizeof(double));
       srand(time(NULL));
       for(i = 0; i < M; i++)
         for(j = 0; j < N; j++)
           A(i,j) = 1+rand()%100;
         for(j = 0; j < N; j++)
           for(k = 0; k < K; k++)
             B(j,k) = 1+rand()%100;
         for(i = 0; i < M; i++)
           for(j = 0; j < K; j++)
             C(i,j) = 0.0;
    } 
    gettimeofday(&tv1, (struct timezone*)0);
    PMATMAT_3(n, A, B, C, p, comm);
    gettimeofday(&tv2, (struct timezone*)0);
    dt1 = (tv2.tv_sec - tv1.tv_sec) * 1000000 + tv2.tv_usec - tv1.tv_usec;
    printf("proc = %d Time = %d\n", proc, dt1);

    if(proc == 0) { 
        free(A);
        free(B);
        free(C);
    }
    MPI_Comm_free(&comm);
    MPI_Finalize();
    return(0);
}

 
