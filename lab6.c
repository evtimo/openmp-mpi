// 7. Дана последовательность натуральных чисел {a0 ... an–1}. 
// Создать OpenMP-приложение для вычисления выражения a0-а1+a2-а3+a4-а5+...

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
  int arr[] = { 1, 6, 8, 13, 10, 17, 2, 8, 9 };
  int size = sizeof(arr)/sizeof(int);
  int positive_sum = 0;
  int negative_sum = 0;
  int i = 0;

// если максимальное количество нитей меньше четырех, то параллельная секция должна выполняться 
// с максимальным количеством нитей, иначе установить количество нитей для параллельной секции равным 4
  int max_threads_num = omp_get_max_threads();
  if (max_threads_num < 4) { omp_set_num_threads(max_threads_num);}
  else omp_set_num_threads(4);

  #pragma omp parallel private(i) shared(arr) reduction(+ : positive_sum) reduction(- : negative_sum) 
  {
    #pragma omp for
    for (i = 0; i < size; i=i+2) {
      positive_sum += arr[i];
      if(size > i+1) { negative_sum -= arr[i+1];}
      printf("thread %d out of %d : positive_sum: %d, negative_sum: %d, i = %d\n", omp_get_thread_num(), omp_get_num_threads(), positive_sum, negative_sum, i);
    }
  }
	
  // Ожидается: 1-6+8-13+10-17+2-8+9 = -14
  printf("\nРезультат: %d\n", positive_sum + negative_sum);
  return 0;
}
